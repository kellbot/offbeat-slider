<?php
/**
 * @package Boostrap Slider
 * @version 1.0.5
 * Plugin Name: Bootstrap Slider
 * Adds a [slider] shortcode to create a bootstrap carousel slider in a post, page or custom post type.
 * Images are taken from gallery attached to current post. Forked from  Elio Rivero's ILC Slider
 * Author: Kelly Maguire
 * Version: 1.0
 * Author URI: http://kellbot.com
*/

/**
 * Get slider global settings.
 * @since 1.0.0
 */
global $ilc_slider_settings;
$ilc_slider_settings = get_option('stored_ilc_slider');

/**
 * Set global variable to allow for multiple sliders in a post.
 * @since 1.0.0
 */
$scid = 0;

/**
 * Definition of shortcode
 * @since 1.0.0
 * @return String
 */
function ilc_slider_sc($atts) {

	global $post;
	global $ilc_slider_settings;
	global $scid;
	
	extract(shortcode_atts(array(
		'id' => $post->ID,
		'size' => $ilc_slider_settings['size'],
		'link' => $ilc_slider_settings['link'],
		'controls' =>  $ilc_slider_settings['controls'],
		'transition' => $ilc_slider_settings['transition'],
		'title' => $ilc_slider_settings['title'],
		'exclude' => FALSE,
		'speed' => $ilc_slider_settings['speed'],
		'timeout' => $ilc_slider_settings['timeout'],
		'delay' => $ilc_slider_settings['delay'],
		'random' => $ilc_slider_settings['random'],
	), $atts));

	$args = array(
		'post_type' => 'attachment',
		'post_parent' => $id,
		'numberposts' => -1,
		'exclude' => $exclude,
		'orderby' => 'menu_order', 'order' => 'ASC'
		); 
	$images = get_posts($args);

	return ilc_bootstrap_html($images, $atts);
}

/* Accepts an array of Images (wordpress posts) and returns bootstrap 3 carousel HTML */
function ilc_bootstrap_html($images, $atts) {
	global $ilc_slider_settings;
	
	extract(shortcode_atts(array(
		'id' => $post->ID,
		'size' => $ilc_slider_settings['size'],
		'link' => $ilc_slider_settings['link'],
		'controls' =>  $ilc_slider_settings['controls'],
		'indicators' =>  $ilc_slider_settings['indicators'],
		'transition' => $ilc_slider_settings['transition'],
		'title' => $ilc_slider_settings['title'],
		'exclude' => FALSE,
		'indicators' => true,
		'speed' => $ilc_slider_settings['speed'],
		'timeout' => $ilc_slider_settings['timeout'],
		'delay' => $ilc_slider_settings['delay'],
		'random' => $ilc_slider_settings['random'],
		'width' => $ilc_slider_settings['width'],
	), $atts));


	$slider =  '
<div id="carousel-images" class="carousel slide" data-ride="carousel">';

	if($indicators) {
		$slider .=  '<ol class="carousel-indicators">';
		  	//indicators
		for ($i = 0; $i < sizeof($images); $i++) {
		  	$active = ($i == 0 ? 'class="active"' : '');
			$slider .=  '<li data-target="#carousel-images" data-slide-to="'.$i.'" '.$active.'></li>';
		}

		$slider .= '</ol>';
	}


	if( $width ) 
		$inner_style = 'style="width: '.$width.'px"';
	$slider .= '
  <div class="carousel-inner" role="listbox" '.$inner_style.'>';
  	$active = 'active';
	foreach ( $images as $image ) {
		$img = wp_get_attachment_image_src($image->ID, $size);

		$slider .= '    <div class="item '.$active.'">';
		$slider .= '<img src="'.$img[0].'" alt="...">';
		$slider .= '</div>';
		$active = '';
	}  

	$slider .='
  </div>';

 	if($controls) {
		$slider .=	'<a class="left carousel-control" href="#carousel-images" role="button" data-slide="prev">
	    <span class="fa fa-angle-double-left icon-prev" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-images" role="button" data-slide="next">
	    <span class="fa fa-angle-double-right icon-next" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>';
	}
	$slider .= '</div>';

	return $slider;
}

/**
 * Adds the shortcode.
 * @since 1.0.0
 */
add_shortcode('slider', 'ilc_slider_sc');
add_shortcode('slideshow', 'ilc_slider_sc');

/**
 * Enqueues styles and scripts.
 * @since 1.0.0
 * @return String
 */

function ilc_slider_setup(){
	$ilc_slider_path = plugins_url('/', __FILE__);
	wp_enqueue_style( 'ilc-slider', $ilc_slider_path . 'ilc-slider.css',null, 28);
}
add_action( 'template_redirect', 'ilc_slider_setup' );

require_once( 'ilc-admin.php');

?>
