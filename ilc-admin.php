<?php

/*=== WordPress Admin Options Page ===*/

add_action('contextual_help', 'ilc_slider_contextual_help');
function ilc_slider_contextual_help($contextual_help) {
	
	$screen = $_GET['page'];
	
	if ($screen == plugin_basename(dirname(__FILE__).'/ilc-admin.php')) {
		$contextual_help = "<h5>Need help with ILC Slider options?</h5>";
		$contextual_help .= "<p>When you activated this plugin, you created a new shortcode. A shortcode is simply a piece text that you can write in your pages or posts to produce extra functionalities. In this case, your new shortcode creates a slider using the images that you uploaded to the post or page where you will be writing it.</p><p>To use your new shortcode just type <strong>[slider]</strong> wherever you want it to appear in your post or page and it will do the rest.</p><p>You have the following options when you type your shortcode (in addition to those found in this page):</p><ul><li><strong>size:</strong> the size of your images, choose between 'thumbnail', 'medium' and 'full'. <em>Default value is 'medium'.</em></li><li><strong>link:</strong> set this to 'file' if you want to add a link to the image file when they are being displayed in the slider. <em>Links are not added by default.</em></li><li><strong>controls:</strong> set this to 'true' if you want to display controls to go back and forward in your slider images. <em>Controls are not shown by default.</em></li><li><strong>title:</strong> set this to true to display the image title. <em>Title is not shown by default.</em></li><li><strong>transition:</strong> choose the transition effect between two images. Possible values are: <br/>fade, scrollHorz, scrollVert, scrollUp, scrollDown, scrollLeft, scrollRight, slideX, slideY, shuffle, turnUp, turnDown, turnLeft, turnRight, zoom, fadeZoom, blindX, blindY, blindZ, growX, growY, curtainX, curtainY, cover, uncover, toss, wipe. <em>Default transition is scrollHorz.</em></li>
		<li><strong>speed:</strong> enter a number in miliseconds to define the speed of the transition animation. A higher number would animate the images entering the slower than a smaller number.</li>
		<li><strong>timeout:</strong> enter a number in miliseconds to define how long the image is still before it leaves and the next one enters.</li>
		<li><strong>delay:</strong> enter a number in miliseconds to define the initial delay before the first transition begins. The first image is displayed during this delay.</li>
		<li><strong>random:</strong> set this to 'true' in your shortcode to randomize the order of the images to be displayed. If you set a global option to randomize images in this page and want to override it in the shortcode, set random to 'false'. <em>The order is NOT random by default.</em></li>
		<li><strong>exclude:</strong> set this attribute to the ID numbers of the images you want to exclude. You can find this ID number here: go to the Gallery of your post or page using the <strong>Add an image</strong> button. Show the image whose ID number you're trying to locate. Below the Link URL field you will find three buttons. Click the one labeled Post URL and at the end of the string you will see the ID number. <em>By default, no images are excluded.</em></li></ul><p>Here's an example of a slider displaying images in the medium size with a link to each image file, controls and excluding the images with ID number 23 and 27</p><code>[slider size=\"medium\" link=\"file\" controls=\"true\" exclude=\"23,27\"]</code><p>In this settings page, you can change the default values for the three first parameters described above.</p>";
		
	}
	return $contextual_help;
}

function ilc_slider_options(){
	$stored_ilc_slider = get_option('stored_ilc_slider');
	$name_ilc_slider = array(
		'size' => FALSE,
		'link' => FALSE,
		'controls' => FALSE,
		'transition' => array(
					'fade' => 'fade',
					'scrollHorz' => 'scrollHorz',
					'scrollVert' => 'scrollVert',
					'scrollUp' => 'scrollUp',
					'scrollDown' => 'scrollDown',
					'scrollLeft' => 'scrollLeft',
					'scrollRight' => 'scrollRight',
					'slideX' => 'slideX',
					'slideY' => 'slideY',
					'shuffle' => 'shuffle',
					'turnUp' => 'turnUp',
					'turnDown' => 'turnDown',
					'turnLeft' => 'turnLeft',
					'turnRight' => 'turnRight',
					'zoom' => 'zoom',
					'fadeZoom' => 'fadeZoom',
					'blindX' => 'blindX',
					'blindY' => 'blindY',
					'blindZ' => 'blindZ',
					'growX' => 'growX',
					'growY' => 'growY',
					'curtainX' => 'curtainX',
					'curtainY' => 'curtainY',
					'cover' => 'cover',
					'uncover' => 'uncover',
					'toss' => 'toss',
					'wipe' => 'wipe'
				),
		'title' => FALSE,
		'exclude' => FALSE,
		'speed' => FALSE,
		'timeout' => FALSE,
		'delay' => FALSE,
		'random' => FALSE,
	);	
	foreach($name_ilc_slider as $key => $val){
		$name[$key] = $key;
		$value[$key] = $stored_ilc_slider[$key];
	}

	if($_POST['ilc_slider_submit'] == 'submit') {
		echo '<div class="updated"><p><strong>Slider settings saved.</strong></p></div>';
		foreach($name_ilc_slider as $key => $val){
			$stored_ilc_slider[$key] = $_POST[$name[$key]];
			$value[$key] = $_POST[$name[$key]];
		}
		update_option('stored_ilc_slider', $stored_ilc_slider);
	}

?>
	
<div class="wrap">
<h2><?php _e('ILC Slider Settings'); ?></h2>

<p>ILC Slider creates a slider on your post, page using the images attached to the current post.</p>

<form id="ilc_slider_settings" method="post" action="<?php echo esc_url(str_replace( '%7E', '~', $_SERVER['REQUEST_URI'])); ?>">
<input type="hidden" name="ilc_slider_submit" value="submit" />

<table class="form-table">
	<tr valign="top">
		<th scope="row">
			<h3>Default size</h3>
			<small><?php _e('Set the default value for size parameter.');?></small>
		</th>
				
		<td><br/><br/>
		<p>
				<input type="radio" name="<?php echo $name['size'];?>" value="thumbnail" <?php if($value['size'] == 'thumbnail') echo "checked=\"checked\""; ?> id="size_t"/>
				<label for="size_t">thumbnail</label>
				<br/>
				<input type="radio" name="<?php echo $name['size'];?>" value="medium" <?php if($value['size'] == 'medium') echo "checked=\"checked\""; ?> id="size_m"/>
				<label for="size_m">medium</label>
                                <br/>
                                <input type="radio" name="<?php echo $name['size'];?>" value="large" <?php if($value['size'] == 'large') echo "checked=\"checked\""; ?> id="size_l"/>
                                <label for="size_l">large</label>
				<br/>
				<input type="radio" name="<?php echo $name['size'];?>" value="full" <?php if($value['size'] == 'full') echo "checked=\"checked\""; ?> id="size_f"/>
				<label for="size_f">full</label>
				<br/>
				
			</p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<h3>Add link to each image</h3>
			<small><?php _e('Check this if you want to add a link to each displayed image file.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="checkbox" name="<?php echo $name['link'];?>" value="true" <?php if($value['link']) echo "checked=\"checked\""; ?> id="link"/>
			<label for="link">Add link</label>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row">
			<h3>Add controls</h3>
			<small><?php _e('Checking this will display controls to go back and forward in your slider.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="checkbox" name="<?php echo $name['controls'];?>" value="true" <?php if($value['controls']) echo 'checked="checked"'; ?> id="controls"/>
			<label for="controls">Add back and forward controls</label>
		</td>
	</tr>
	
	<tr valign="top">
		<th scope="row">
			<h3>Show image title</h3>
			<small><?php _e('Checking this will display the title that you assigned to your image.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="checkbox" name="<?php echo $name['title'];?>" value="true" <?php if($value['title']) echo 'checked="checked"'; ?> id="title"/>
			<label for="title">Show title</label>
		</td>
	</tr>
	
	<tr valign="top">
		<th scope="row">
			<h3>Select default transition</h3>
			<small><?php _e('Choose the transition that will be applied.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<select name="<?php echo $name['transition']; ?>" id="transition">
				<?php foreach ($name_ilc_slider['transition'] as $k => $v) { ?>
				<option <?php if ($value['transition'] == $k) echo 'selected="selected"'; ?>>
					<?php echo $k; ?>
				</option>
				<?php } ?>
			</select>
			<label for="transition">Transition</label>
		</td>
	</tr>
	
	<tr valign="top">
		<th scope="row">
			<h3>Enter default speed</h3>
			<small><?php _e('Enter the speed of the transition.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="text" name="<?php echo $name['speed']; ?>" id="speed" value="<?php echo $value['speed']; ?>">
			<label for="speed">miliseconds</label>
		</td>
	</tr>
	
	<tr valign="top">
		<th scope="row">
			<h3>Enter default pause</h3>
			<small><?php _e('Enter the length of the pause between transitions.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="text" name="<?php echo $name['timeout']; ?>" id="timeout" value="<?php echo $value['timeout']; ?>">
			<label for="timeout">miliseconds (enter 0 to disable auto play)</label>
		</td>
	</tr>
	
	<tr valign="top">
		<th scope="row">
			<h3>Enter initial delay</h3>
			<small><?php _e('Enter the length of the initial delay before the first transition.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="text" name="<?php echo $name['delay']; ?>" id="delay" value="<?php echo $value['delay']; ?>">
			<label for="delay">miliseconds (value can be negative)</label>
		</td>
	</tr>

	<tr valign="top">
		<th scope="row">
			<h3>Randomize image order</h3>
			<small><?php _e('Checking this will display the images in a random order.');?></small>
		</th>
				
		<td><br/><br/><br/>
			<input type="checkbox" name="<?php echo $name['random'];?>" value="true" <?php if($value['random']) echo 'checked="checked"'; ?> id="random"/>
			<label for="title">Randomize order</label>
		</td>
	</tr>
	
</table>


<p class="submit">
<input type="submit" name="Submit" value="<?php _e('Update settings') ?>" class="button-primary" />
</p>
</form>

</div>

<?php
}
/*
 * Creates Settings link on plugins list page.
 */
function ilc_slider_settings($links, $file) {
	if ($file == plugin_basename(dirname(__FILE__).'/ilc-slider.php')) {
		foreach($links as $k=>$v) {
			if (strpos($v, 'plugin-editor.php?file=') !== false)
				unset($links[$k]);
		}
		$links[] = "<a href='options-general.php?page=".plugin_basename(dirname(__FILE__))."/ilc-admin.php'><b>Settings</b></a>";
	}
	return $links;
}

/*
 * Adds Settings link on plugins page. Create options page on wp-admin.
 */
function ilc_slider_plugin_menu() {
	add_filter( 'plugin_action_links', 'ilc_slider_settings', -10, 2);
	add_options_page('ILC Slider', 'ILC Slider', 8, __FILE__, 'ilc_slider_options');
}
add_action('admin_menu', 'ilc_slider_plugin_menu');

/*
 * When the plugin is activated, we will setup some options on the database
 */
function ilc_slider_activate(){
//	add_option("ilc_slider_path", WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)));
	add_option("ilc_slider_path", plugins_url('/', __FILE__));
	add_option('stored_ilc_slider', array(
		'size' => 'medium',
		'link' => FALSE,
		'controls' => FALSE,
		'transition' => 'fade',
		'title' => FALSE,
		'exclude' => FALSE,
		'speed' => 800,
		'timeout' => 2000,
		'delay' => -8000,
		'random' => 0,
	));
}

/*
 * When the plugin is deactivated, we will (not) erase all options from database
 */
function ilc_slider_deactivate(){
	//delete_option('stored_ilc_slider', $stored_ilc_slider);
}

register_activation_hook(	dirname(__FILE__).'/ilc-slider.php', 'ilc_slider_activate');
register_deactivation_hook(	dirname(__FILE__).'/ilc-slider.php', 'ilc_slider_deactivate');
?>
